package models.exception;

public class HttpException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private util.Error error;
	
	public HttpException(String msg){
		super(msg);
		this.error = new util.Error(msg);
	}

	public HttpException(String msg, String field){
		super(msg);
		this.error = new util.Error(field, msg);
	}
	
	public util.Error getError() {
		return error;
	}

	public void setError(util.Error error) {
		this.error = error;
	}
	
	
}
