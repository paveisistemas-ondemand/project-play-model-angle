package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name="clients")
public class Client extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	
	@Required
	private String clientName;
	
	public static Model.Finder<Integer, Client> finder = new Model.Finder<Integer, Client>(Integer.class, Client.class);
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	public static boolean isRepeatedClient(String name){
		boolean result = false;
		
		List<Client> sameNameClients = Client.finder.select("client_name").where().eq("client_name", name).findList();
		
		if(!sameNameClients.isEmpty()){
			result = true;
		}
		
		return result;
	}
	
	public static boolean isClientInUseByUser(Integer id){
		boolean result = false;
		
		List<User> usersUsingClient = User.finder.query().where().eq("client_id", id).findList();
		
		if(!usersUsingClient.isEmpty()){
			result = true;
		}
		
		return result;
	}
	
}
