package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.dto.UserDTO;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import util.MD5;

@Entity
@Table(name="users")
public class User extends Model {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	
	@Required
	private String role;
	
	@Required
	@ManyToOne
	private Client client;
	
	@Required
	@Column
	private String userName;
	
	@Required
	private String email;
	
	@Required
	private String password;
	
	public static Model.Finder<Integer, User> finder = new Model.Finder<Integer, User>(Integer.class, User.class);

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public static boolean isEmailInUse(String email){
		boolean result = false;
		
		List<User> sameEmailsUsers = User.finder.select("email").where().eq("email", email).findList();
		
		if(!sameEmailsUsers.isEmpty()){
			result = true;
		}
		
		return result;
	}
	
	public static String digest(String password){
		return MD5.digest(password);
	}
	
	public static boolean isUserAdmin(Integer id){
		boolean result = false;
		
		User user = User.finder.byId(id);
		
		if(user.getRole().equals("admin")){
			result = true;
		}
		
		return result;
	}
	
	public static List<UserDTO> all(User user){
		
		String whereClause = "";
		
		if(!user.getRole().equals("global")){
			Integer clientId = user.getClient().getId();
			
			whereClause += "AND client_id = " + clientId;
		}
		
		String sql = "SELECT "
						+ "users.id, "
						+ "users.role, "
						+ "clients.client_name, "
						+ "users.user_name, "
						+ "users.email "
						+ "FROM users "
						+ "JOIN clients ON clients.id=users.client_id "
						+ "WHERE users.role != 'global' " + whereClause + " "
						+ "ORDER BY users.user_name";
	
		RawSql rawSql = RawSqlBuilder.unparsed(sql)
				.columnMapping("id", "id")
				.columnMapping("role", "role")
				.columnMapping("client_name", "client")
				.columnMapping("user_name", "userName")
				.columnMapping("email", "email")
				.create();
		
		Query<UserDTO> query = Ebean.find(UserDTO.class);
		query.setRawSql(rawSql);
	
		return query.findList();
	}
}
