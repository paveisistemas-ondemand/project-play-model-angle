package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Client;
import models.User;
import models.exception.HttpException;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import actions.SecuredAction;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;

public class Clients extends Controller {
	
	@With(SecuredAction.class)
	public static Result all(){
		
		Result result = null;
		
		List<Client> clients = new ArrayList<Client>();
		
		User user = (User) Http.Context.current().args.get("currentUser");
		
		if(user.getRole().equals("global")){
			clients = Client.finder.orderBy("clientName").findList();
		} else {
			clients = Client.finder.orderBy("clientName").where().eq("id", user.getClient().getId()).findList();
		}
		
		if(clients != null){
			result = ok(Json.toJson(clients));
		} else{
			result = internalServerError();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	public static Result find(Integer id){
		
		Result result = null;
		
		Client client = Client.finder.byId(id);
		
		if(client != null){
			result = ok(Json.toJson(client));
		} else{
			result = notFound();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result save(){
		
		Result result = null;
		
		Ebean.beginTransaction();
		
		try{
			JsonNode jsonNode = request().body().asJson();
			Client newClient = Json.fromJson(jsonNode, Client.class);
			
			if(Client.isRepeatedClient(newClient.getClientName())){
				throw new HttpException("Cliente já cadastrado.", "clientName");
			}
			
			newClient.save();
			
			Ebean.commitTransaction();
			
			result = ok();
			
		} catch(Exception e){
			e.printStackTrace();
			Ebean.rollbackTransaction();
			if(e instanceof HttpException){
				HttpException e1 = (HttpException) e;
				result = badRequest(Json.toJson(e1.getError()));
			} else{
				result = internalServerError(Json.toJson(new util.Error("general", "Ops, ocorreu um erro. Tente novamente")));
			}
		} finally{
			Ebean.endTransaction();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result update(int id){
		
		Result result = null;
		
		Ebean.beginTransaction();
		
		try{
			JsonNode jsonNode = request().body().asJson();
			Client updatedClient = Json.fromJson(jsonNode, Client.class);
			
			Client oldClient = Client.finder.byId(id);
			
			//se o nome não mudou, não verificar se já existe
			if(!updatedClient.getClientName().equals(oldClient.getClientName())){
				if(Client.isRepeatedClient(updatedClient.getClientName())){
					throw new HttpException("Cliente já cadastrado", "clientName");
				}
			}
			
			oldClient = updatedClient;
			oldClient.update();
			
			Ebean.commitTransaction();
			
			result = ok();
					
		} catch(Exception e){
			e.printStackTrace();
			Ebean.rollbackTransaction();
			if(e instanceof HttpException){
				HttpException e1 = (HttpException) e;
				result = badRequest(Json.toJson(e1.getError()));
			} else{
				result = internalServerError(Json.toJson(new util.Error("general", "Ops, ocorreu um erro. Tente novamente")));
			}
		} finally{
			Ebean.endTransaction();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	public static Result delete(Integer id){
		
		Result result = null;
		
		Ebean.beginTransaction();
		
		try{
			
			if(Client.isClientInUseByUser(id)){
				throw new HttpException("Impossível realizar a ação. O cliente está relacionado com pelo menos 1 usuário.");
			}
			
			Client.finder.byId(id).delete();
			
			Ebean.commitTransaction();
			
			result = ok();
			
		} catch(Exception e){
			e.printStackTrace();
			Ebean.rollbackTransaction();
			if(e instanceof HttpException){
				HttpException e1 = (HttpException) e;
				result = badRequest(Json.toJson(e1.getError()));
			} else{
				result = internalServerError(Json.toJson(new util.Error("Ops, ocorreu um erro. Tente novamente")));
			}
		} finally{
			Ebean.endTransaction();
		}
		
		return result;
	}
}
