package controllers;

import java.util.List;

import models.User;
import models.dto.UserDTO;
import models.exception.HttpException;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import util.MD5;
import actions.SecuredAction;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;

public class Users extends Controller {
	
	@With(SecuredAction.class)
	public static Result all(){
		
		Result result = null;
		
		User user = (User) Http.Context.current().args.get("currentUser");
		
		List<UserDTO> users = User.all(user);
		
		if(users != null){
			result = ok(Json.toJson(users));
		} else{
			result = internalServerError();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	public static Result find(Integer id){
		
		Result result = null;
		
		User user = User.finder.byId(id);
		
		if(user != null){
			result = ok(Json.toJson(user));
		} else{
			result = notFound();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result save(){
		
		Result result = null;
		
		User user = (User) Http.Context.current().args.get("currentUser");
		
		Ebean.beginTransaction();
		
		try{
			
			JsonNode jsonNode = request().body().asJson();
			User newUser = Json.fromJson(jsonNode, User.class);
			
			if(User.isEmailInUse(newUser.getEmail())){
				throw new HttpException("Este email já está sendo usado.", "email");
			}
			
			if(user.getRole().equals("admin")){
				newUser.setClient(user.getClient());
			}
			
			newUser.setPassword(User.digest(newUser.getPassword()));
			
			newUser.save();
			
			Ebean.commitTransaction();
			
			result = ok();
			
		} catch(Exception e){
			e.printStackTrace();
			Ebean.rollbackTransaction();
			if(e instanceof HttpException){
				HttpException e1 = (HttpException) e;
				result = badRequest(Json.toJson(e1.getError()));
			} else{
				result = internalServerError(Json.toJson(new util.Error("general", "Ops, ocorreu um erro. Tente novamente")));
			}
		} finally{
			Ebean.endTransaction();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result update(Integer id){
		
		Result result = null;
		
		Ebean.beginTransaction();
		
		try{
			
			JsonNode jsonNode = request().body().asJson();
			User updatedUser = Json.fromJson(jsonNode, User.class);
			
			User oldUser = User.finder.byId(id);
			
			if(!oldUser.getEmail().equals(updatedUser.getEmail())){
				if(User.isEmailInUse(updatedUser.getEmail())){
					throw new HttpException("Este email já está sendo usado", "email");
				}
			}
			
			updatedUser.setPassword(User.digest(updatedUser.getPassword()));
			
			oldUser = updatedUser;
			oldUser.update();
			
			Ebean.commitTransaction();
			
			result = ok();
			
		} catch(Exception e){
			e.printStackTrace();
			Ebean.rollbackTransaction();
			if(e instanceof HttpException){
				HttpException e1 = (HttpException) e;
				result = badRequest(Json.toJson(e1.getError()));
			} else{
				result = internalServerError(Json.toJson(new util.Error("general", "Ops, ocorreu um erro. Tente novamente")));
			}
		} finally{
			Ebean.endTransaction();
		}
		
		return result;
	}
	
	@With(SecuredAction.class)
	public static Result delete(Integer id){
		
		Result result = null;
		
		Ebean.beginTransaction();
		
		try{
			
			User.finder.byId(id).delete();
			Ebean.commitTransaction();
			
			result = ok();
			
		} catch(Exception e){
			e.printStackTrace();
			Ebean.rollbackTransaction();
			result = internalServerError(Json.toJson(new util.Error("Ops, ocorreu um erro. Tente novamente")));
		} finally{
			Ebean.endTransaction();
		}
		
		return result;
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result login(){
		Result result = null;
		
		JsonNode jsonNode = request().body().asJson();
		User login = Json.fromJson(jsonNode, User.class);
		
		try{
			
			User user = User.finder.query().where().eq("email", login.getEmail()).findUnique();
			if(user == null){
				throw new HttpException("Usuário não encontrado.");
			}
			
			if(!user.getPassword().equals(MD5.digest(login.getPassword()))){
				throw new HttpException("Usuário não encontrado.");
			}
			
			result = ok(Json.toJson(user));
			
		} catch(Exception e){
			result = badRequest();
		}
		
		return result;
	}
	
}
