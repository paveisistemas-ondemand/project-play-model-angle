import java.lang.reflect.Method;
import java.util.List;

import models.Client;
import models.User;
import actions.CorsAction;
import play.Application;
import play.GlobalSettings;
import play.mvc.Action;
import play.mvc.Http.Request;

public class Global extends GlobalSettings{
  
	  @Override
	  public Action<?> onRequest(Request arg0, Method arg1) {
		  return new CorsAction();
	  }
	  
	  @Override
		public void onStart(Application arg0) {
			// TODO Auto-generated method stub
			super.onStart(arg0);
			
			System.out.println("Estou no start");
			
			List<Client> clients = Client.finder.findList();
			
			if(clients == null || (clients != null && clients.isEmpty())){
				
				try{
					
					Client client1 = new Client();
					client1.setClientName("Pavei Sistemas");
					client1.save();
					
					User user1 = new User();
					user1.setClient(client1);
					user1.setEmail("administrador@paveisistemas.com.br");
					user1.setPassword("e7a883e1060389ea855c9ad66391d31a");
					user1.setRole("global");
					user1.setUserName("Pavei");
					user1.save();
					
					Client client2 = new Client();
					client2.setClientName("Restaurante do João");
					client2.save();
					
					User user2 = new User();
					user2.setClient(client2);
					user2.setEmail("joao@joao.com");
					user2.setPassword("827ccb0eea8a706c4c34a16891f84e7b");
					user2.setRole("admin");
					user2.setUserName("João");
					user2.save();
					
				} catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
		}
  
}
