package actions;

import models.User;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

public class SecuredAction extends Action.Simple {
	
	public F.Promise<Result> call(Http.Context ctx) throws Throwable {
		String[] token = getTokenFromHeader(ctx).split(":");
		if (token != null) {
			User user = User.finder.where().eq("email", token[0]).eq("password", token[1]).findUnique();
			if (user != null) {
				ctx.request().setUsername(user.getPassword());
				ctx.args.put("currentUser", user);
				return delegate.call(ctx);
			}
		}
		Result unauthorized = Results.unauthorized("unauthorized");
		return F.Promise.pure(unauthorized);
	}

	private String getTokenFromHeader(Http.Context ctx) {
		String[] authTokenHeaderValues = ctx.request().headers().get("Authorization");
		if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
			return authTokenHeaderValues[0];
		}
		return null;
	}
}
