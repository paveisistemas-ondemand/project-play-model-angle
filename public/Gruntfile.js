module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg : require('./package.json'),
		replace : {
			dev : {
				src : [ 'app/**/*.js' ],
				overwrite : true,
				replacements : [ {
					from : '<%= pkg.url_production %>',
					to : '<%=  pkg.url_dev %>'
				} ]

			},

			prod : {
				src : [ 'app/**/*.js' ],
				overwrite : true,
				replacements : [ {
					from : '<%= pkg.url_dev %>',
					to : '<%=  pkg.url_production %>'
				} ]

			}

		}

	})

	;

	// Load tasks.

	grunt.loadNpmTasks('grunt-text-replace');

	// Default task.
	grunt.registerTask('dev', 'replace:dev');
	grunt.registerTask('prod', 'replace:prod');

};
