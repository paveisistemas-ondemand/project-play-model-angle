var URL_LOCATION = window.location.host;
// 54.94.219.78:8080
// 192.168.43.54:8080

var app = angular.module('smartkilo', [	'ngRoute',
			                            'ngResource',
			                            'ngAnimate',
			                            'ngStorage',
			                            'ngTable',
			                            'ngTableExport',
			                            'flash',
			                            'ui.bootstrap',
			                            'ui.utils.masks',
			                            'angular-loading-bar',
			                            'smartkilo.index',
			                            'smartkilo.login',
			                            'smartkilo.clients',
			                            'smartkilo.users'
]);

app.config(['$routeProvider', function($routeProvider) {
	
	$routeProvider.otherwise({
		redirectTo : '/users'
	});
	
	$routeProvider.when('/login', {
		templateUrl: 'app/login/index.html',
		requiredLogin: false
	});
	
	// CLIENTS ===============================================================
	$routeProvider.when('/clients', {
		templateUrl: 'app/clients/index.html',
		controller: 'ClientIndexCtrl',
		requiredLogin: true,
		requiredGlobal: true,
		resolve: {
			clients: function($q, $location, Client){
				
				var defer = $q.defer();
				
				Client.query(function(result){
					defer.resolve(result);
				}, function(error){
					defer.reject($location.path('/login'));
				});
				
				return defer.promise;
			}
		}
	});
	
	$routeProvider.when('/clients/new', {
		templateUrl: 'app/clients/new.html',
		controller: 'ClientNewCtrl',
		requiredLogin: true,
		requiredGlobal: true
	});
	
	$routeProvider.when('/clients/:id/edit', {
		templateUrl: 'app/clients/edit.html',
		controller: 'ClientEditCtrl',
		requiredLogin: true,
		requiredGlobal: true,
		resolve: {
			client: function($q, $location, Client, $route){

				var defer = $q.defer();
				
				Client.get({id: $route.current.params.id}, function(result){
					defer.resolve(result);
				}, function(error){
					defer.reject($location.path('/login'));
				});
				
				return defer.promise;
			}
		}
	});
	

	//USERS ================================================================
	$routeProvider.when('/users', {
		templateUrl: 'app/users/index.html',
		controller: 'UserIndexCtrl',
		requiredLogin: true,
		requiredAdmin: true,
		resolve: {
			clients: function($q, $location, Client){
				
				var defer = $q.defer();
				
				Client.query(function(result){
					defer.resolve(result);
				}, function(error){
					defer.reject($location.path('/login'));
				});
				
				return defer.promise;
			},
			users: function($q, $location, User){
				
				var defer = $q.defer();
				
				User.query(function(result){
					defer.resolve(result);
				}, function(error){
					defer.reject($location.path('/login'));
				});
				
				return defer.promise;
			}
		}
	});

	$routeProvider.when('/users/new', {
		templateUrl: 'app/users/new.html',
		controller: 'UserNewCtrl',
		requiredLogin: true,
		requiredAdmin: true,
		resolve: {
			clients: function($q, $location, Client){
				
				var defer = $q.defer();
				
				Client.query(function(result){
					defer.resolve(result);
				}, function(error){
					defer.reject($location.path('/login'));
				});
				
				return defer.promise;
			}
		}
	});
	
	$routeProvider.when('/users/:id/edit', {
		templateUrl: 'app/users/edit.html',
		controller: 'UserEditCtrl',
		requiredLogin: true,
		requiredAdmin: true,
		resolve: {
			clients: function($q, $location, Client){
				
				var defer = $q.defer();
				
				Client.query(function(result){
					defer.resolve(result);
				}, function(error){
					defer.reject($location.path('/login'));
				});
				
				return defer.promise;
			},
			user: function($q, $location, $route, User){
				
				var defer = $q.defer();
				
				User.get({id: $route.current.params.id}, function(result){
					defer.resolve(result);
				}, function(error){
					defer.reject($location.path('/login'));
				});
				
				return defer.promise;
			}
		}
	});

	
	
	
}]);

app.run(function($rootScope, $location, $http, Session){
	
	var session = Session.get();
	
	if(Session.get().password){
		$http.defaults.headers.common.Authorization = session.email + ":" + session.password;
	}
	
	$rootScope.$on('$routeChangeStart', function(event, nextRoute, currentRoute){
		
		if(nextRoute.requiredLogin && !Session.get().password){
			$location.path('/login');
		} else{
			$http.defaults.headers.common.Authorization = session.email + ":" + session.password;
					
			//$location.path('/users');
								
		}
	});
	
});

app.factory('Session', function($localStorage) {
	var functions = {};
	
    functions.clean = function(){
    	$localStorage.$reset();
    }
        
    functions.put = function(obj){
    	$localStorage.$reset(obj);
    }   
        
    functions.get = function(){
    	return $localStorage;
    }
    
    return functions;
});

app.factory('ResponsiveWindow', function ($window) {

	var functions = {};
	
	
	functions.isXs = function(winWidth){
		return winWidth < 768;
	}
	
	functions.isSm = function(winWidth){
		return winWidth >= 768 && winWidth < 992;
	}

	functions.isMd = function(winWidth){
		return winWidth >= 992 && winWidth < 1200;
	}

	functions.isLg = function(winWidth){
		return winWidth >= 1200;
	}
	
	return functions;
	
});	


app.factory('Client', function($resource) {
	return $resource('http://'+ URL_LOCATION +'/clients/:id', {id : '@_id'}, {update : {method : 'PUT'}});
});

app.factory('User', function($resource) {
	return $resource('http://'+ URL_LOCATION +'/users/:id', {id : '@_id'}, {update : {method : 'PUT'} });
});


