(function(root, factory) {
	if (typeof module !== 'undefined' && module.exports) {
		// CommonJS
		module.exports = factory(root, require('angular'));
	} else if (typeof define === 'function' && define.amd) {
		// AMD
		define([ 'angular' ], function(react, angular) {
			return (root.angularFlash = factory(root, angular));
		});
	} else {
		// Global Variables
		root.angularFlash = factory(root, root.angular);
	}
}
		(
				this,
				function(window, angular) {
					return angular
							.module('flash', [])
							.factory(
									'flash',
									[
											'$rootScope',
											'$timeout',
											function($rootScope, $timeout) {
												var messages = [];
												var reset;
												var cleanup = function() {
													$timeout.cancel(reset);
													reset = $timeout(function() {
														messages = [];
													});
												};
												var emit = function() {
													$rootScope.$emit(
															'flash:message',
															messages, cleanup);
												};
												$rootScope
														.$on(
																'$locationChangeSuccess',
																emit);
												var asMessage = function(level,
														text) {
													if (!text) {
														text = level;
														level = 'success';
													}
													return {
														level : level,
														text : text
													};
												};
												var asArrayOfMessages = function(
														level, text) {
													if (level instanceof Array)
														return level
																.map(function(
																		message) {
																	return message.text ? message
																			: asMessage(message);
																});
													return text ? [ {
														level : level,
														text : text
													} ] : [ asMessage(level) ];
												};
												var flash = function(level,
														text) {
													emit(messages = asArrayOfMessages(
															level, text));
												};
												[ 'error', 'warning', 'info',
														'success' ]
														.forEach(function(level) {
															flash[level] = function(
																	text) {
																flash(level,
																		text);
															};
														});
												return flash;
											} ])
							.directive(
									'flashMessages',
									['$timeout', function( $timeout) {
										var directive = {
											restrict : 'EA',
											replace : true
										};
										directive.template = "<div id='flash-messages' ng-show='showFlash'>" +
																"<div ng-repeat='m in messages' class='alert alert-{{m.level}}'>" +
																	"<button ng-click='closeMessage(m)' class='close' type='button'>" +
																		"<span aria-hidden='true'>×</span>" +
																		"<span class='sr-only'>Close</span>" +
																	"</button>" +
																	"<div>" +
																		"<span class='ng-binding ng-scope'>{{m.text}}" +
																		"</span>" +
																	"</div>" +
																"</div>" +
															"</div>";
										
//										directive.template = '<ol id="flash-messages">'
//												+ '<li ng-repeat="m in messages" class="alert alert-{{m.level}}">{{m.text}}</li>'
//												+ '</ol>';
										directive.controller = [
												'$scope',
												'$rootScope',
												'$element',
												
												function($scope, $rootScope, $element) {
													var lastTimeout = null;
													$scope.mostrar = false;
													
													
													$scope.closeMessage = function(m){
														$scope.messages.splice($scope.messages.indexOf(m), 1);
													}
													
													$rootScope
															.$on(
																	'flash:message',
																	function(
																			_,
																			messages,
																			done) {
																		$scope.messages = messages;
																		done();
																		$scope.showFlash = true;
																		
																		if (lastTimeout){
																			$timeout.cancel(lastTimeout)
																		}
																		lastTimeout = 
																		$timeout(function(){
																			$scope.showFlash = false;
																		}, 5000)
																		
																	});
													
													
													
													
												} ];
										return directive;
									} ]);
				}));