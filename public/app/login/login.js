var login = angular.module('smartkilo.login',[]);

login.factory("FormValidateLogin", function() {
	return {
		validate : function(user) {
			var error = {};
			
			if (!user.email) {
				error.email = "O campo não pode ser vazio.";
			} else{
				var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				if(!regex.test(user.email)){
					error.email = "O email é inválido.";
				}
			}
			
			if (!user.password) {
				error.password = "O campo não pode ser vazio.";
			} else if(user.password.length < 4){
				error.password = "O campo deve ter no mínimo 4 caracteres.";
			}
			
			error.hasError = Object.keys(error).length !== 0;
			
			return error;
		}
	}
});

login.controller('LoginIndexCtrl',
function($scope, $location, $timeout, $rootScope, $window, Session, User, FormValidateLogin, $http){
	
	$rootScope.$broadcast('changeTitle', "login");
	
	$scope.user = new User();
	$scope.error = {};
	
	$scope.login = function(){
		
		$scope.error = FormValidateLogin.validate($scope.user);
		
		if(!$scope.error.hasError){
			
			$scope.successMessage = "";
			$scope.errorMessage = "";
			
			$http.post('http://'+URL_LOCATION+'/users/login', $scope.user)
				.success(function(result){
					$scope.successMessage = "Login efetuado com sucesso";
					Session.put(result);
					$timeout(function(){
						$location.path("/");
					}, 1500);
				})
				.error(function(error){
					$scope.errorMessage = "Usuário e/ou senha inválido(s)";
				});
		}
	}
	
	$scope.getYear = function() {
		var d = new Date();
		var n = d.getFullYear();
		return n;
	}
	
});