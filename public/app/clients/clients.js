var clients = angular.module('smartkilo.clients',[]);

clients.factory("FormValidateClient", function() {
	return {
		validate : function(client) {
			var error = {};

			if (!client.clientName) {
				error.clientName = "O campo não pode ser vazio.";
			}
			
			error.hasError = Object.keys(error).length !== 0;
			
			return error;
		}
	}
});

clients.controller('ClientIndexCtrl',
function($scope, $rootScope, $window, ngTableParams, Client, clients, flash) {

	$rootScope.$broadcast('changeTitle', "Clientes");
	
	$scope.clients = clients;
	
	$scope.tableParams = new ngTableParams({
		page : 1,
		count : 8
	}, {
		total : $scope.clients.length,
		counts : [],
		$scope : {$data : {}},
		getData : function($defer, params) {
			
			params.total($scope.clients.length);
			
			if (params.total() <= params.count()) {
				params.page(1);
			}
			
			$defer.resolve($scope.clients.slice((params.page() - 1) * params.count(), params.page() * params.count()));
		}
	});

	$scope.delClient = function(client) {
		
		var confirm = $window.confirm('Deseja remover o cliente?');
		
		if(confirm){
			Client.remove({id : client.id}, function() {
				$scope.clients.splice($scope.clients.indexOf(client), 1);
				$scope.tableParams.reload();
				flash("Cliente removido com sucesso");
			}, function(error) {
				flash("danger",error.data.message);
			})
		}
		
	};
});

clients.controller('ClientNewCtrl',
function($scope, $location, $rootScope, $window, ngTableParams, FormValidateClient, Client, flash) {

	$rootScope.$broadcast('changeTitle', "Clientes");

	$scope.client = new Client();
	$scope.error = {};
	
	$scope.cancel = function(){
		var confirm = $window.confirm('Deseja cancelar?');
		
		if(confirm){
			$location.path('/clients');
		}
	}
	
	$scope.save = function() {
		
		$scope.error = FormValidateClient.validate($scope.client);
		if($scope.error.hasError){
			flash("danger", "Verifique os campos abaixo");
		}
		
		if (!$scope.error.hasError) {
			Client.save($scope.client, function() {
				$location.path("/clients");
				flash("Cliente adicionado com sucesso");
			}, function(error) {
				if(error.data.field == "general"){
					flash("danger", error.data.message);
				} else{
					flash("danger", "Verifique os campos abaixo");
					$scope.error[error.data.field] = error.data.message;
				}
				
			});
		}
	}
});

clients.controller('ClientEditCtrl',
function($scope, $location, $rootScope, $window, FormValidateClient, Client, client, flash) {

	$rootScope.$broadcast('changeTitle', "Clientes");

	$scope.client = client;
	$scope.error = {};
	
	$scope.cancel = function(){
		var confirm = $window.confirm('Deseja cancelar?');
		
		if(confirm){
			$location.path('/clients');
		}
	}
	
	$scope.update = function() {
		
		$scope.error = FormValidateClient.validate($scope.client);
		if($scope.error.hasError){
			flash("danger", "Verifique os campos abaixo");
		}

		if (!$scope.error.hasError) {
			Client.update({id : $scope.client.id}, $scope.client, function(){
				$location.path("/clients");
				flash("Cliente alterado com sucesso");
			}, function(error) {
				if(error.data.field == "general"){
					flash("danger", error.data.message);
				} else{
					flash("danger", "Verifique os campos abaixo");
					$scope.error[error.data.field] = error.data.message;
				}
			});
		}
	}
});
