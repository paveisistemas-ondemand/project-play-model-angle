var users = angular.module('smartkilo.users',[]);

users.factory("FormValidateUser", function() {
	return {
		validate : function(user) {
			var error = {};
			
			if (!user.client) {
				error.client = "O campo não pode ser vazio.";
			}
			
			if (!user.role) {
				error.role = "O campo não pode ser vazio.";
			}
			
			if (!user.userName) {
				error.userName = "O campo não pode ser vazio.";
			}
			
			if (!user.email) {
				error.email = "O campo não pode ser vazio.";
			} else{
				var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				if(!regex.test(user.email)){
					error.email = "O email é inválido.";
				}
			}
			
			if (!user.password) {
				error.password = "O campo não pode ser vazio.";
			} else if(user.password.length < 4){
				error.password = "O campo deve ter no mínimo 4 caracteres.";
			}
			
			error.hasError = Object.keys(error).length !== 0;
			
			return error;
		}
	}
});

users.controller('UserIndexCtrl',
function($scope, $rootScope, $window, $filter, ngTableParams, User, users, clients, flash){
	
	$rootScope.$broadcast('changeTitle', "Usuários");
	
	$scope.users = users;
	$scope.clients = clients;
	$scope.userFilter = {};
	$scope.tableFilter = {};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            
        count: 8
    }, {
        total: users.length,
        counts: [],
        $scope: { $data: {} },
        getData: function($defer, params) {
        	
        	params.total($scope.users.length);
        	
        	if (params.total() < params.count()) {
				params.page(1);
			}
        	
            $defer.resolve($scope.users.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
	
	$scope.filter = function(){
		$scope.tableFilter.client = $scope.userFilter.client;
	}
	
	$scope.delUser = function(user){
		
		var confirm = $window.confirm('Observação: Ao remover o usuário, serão também removidas suas notificações. Deseja remover o usuário?');
		
		if(confirm){
			User.remove({id: user.id}, function(){
				$scope.users.splice($scope.users.indexOf(user), 1);
				$scope.tableParams.reload();
				flash("Usuário removido com sucesso");
			}, function(error){
				flash("danger", error.data.message);
			});
		}
	}
	
});

users.controller('UserNewCtrl',
function($scope, $location, $rootScope, $window, User, FormValidateUser, clients, flash){

	$rootScope.$broadcast('changeTitle', "Usuários");

	$scope.user = new User();
	$scope.clients = clients;
	if(!$scope.isGlobal()){
		$scope.user.client = clients[0];
	}
	$scope.error = {};
	
	$scope.cancel = function(){
		var confirm = $window.confirm('Deseja cancelar?');
		
		if(confirm){
			$location.path('/users');
		}
	}
	
	$scope.save = function(){
		
		$scope.error = FormValidateUser.validate($scope.user);
		if($scope.error.hasError){
			flash("danger", "Verifique os campos abaixo");
		}
		
		if(!$scope.error.hasError){
			User.save($scope.user, function(){
				$location.path('/users');
				flash("Usuário adicionado com sucesso");
			}, function(error){
				if(error.data.field == "general"){
					flash("danger", error.data.message);
				} else{
					flash("danger", "Verifique os campos abaixo");
					$scope.error[error.data.field] = error.data.message;
				}
			});
		}
	}
});

users.controller('UserEditCtrl',
function($scope, $location, $rootScope, $window, User, FormValidateUser, user, clients, flash){
	
	$rootScope.$broadcast('changeTitle', "Usuários");
	
	$scope.user = user;
	$scope.user.password = "";
	$scope.clients = clients;
	$scope.error = {};
	
	$scope.cancel = function(){
		var confirm = $window.confirm('Deseja cancelar?');
		
		if(confirm){
			$location.path('/users');
		}
	}
	
	$scope.update = function(){
		
		$scope.error = FormValidateUser.validate($scope.user);
		if($scope.error.hasError){
			flash("danger", "Verifique os campos abaixo");
		}
		
		if(!$scope.error.hasError){
			
			User.update({id: $scope.user.id}, $scope.user, function(){
				$location.path('/users');
				flash("Usuário alterado com sucesso");
			}, function(error){
				if(error.data.field == "general"){
					flash("danger", error.data.message);
				} else{
					flash("danger", "Verifique os campos abaixo");
					$scope.error[error.data.field] = error.data.message;
				}
			});
		}
	}
});