var index = angular.module('smartkilo.index',[]);

index.controller('IndexCtrl',
function($scope, $timeout, $rootScope, $location, $window, ResponsiveWindow, Session){
	
	$scope.session = Session.get();
	$scope.isTitleShow = true;
	$scope.showAsideBar = false;
	
	$scope.isXs = function(){
		return ResponsiveWindow.isXs($window.innerWidth);
	}
	
	$scope.isSm = function(){
		return ResponsiveWindow.isSm($window.innerWidth);
	}
	
	$scope.isMd = function(){
		return ResponsiveWindow.isMd($window.innerWidth);
	}
	
	$scope.isLg = function(){
		return ResponsiveWindow.isLg($window.innerWidth);
	}
	
	$scope.verifyWindow = function(){
		
		if($scope.isXs()){
			$scope.asideColapsed = null;
		} else{
			$scope.asideColapsed = {'margin-left': 0};
		}
		
	}
	$scope.verifyWindow();
	
	window.addEventListener("resize", function(){
		
		$timeout(function(){
			$scope.verifyWindow();
		});
		
//		$scope.$apply(function () {
//			$scope.verifyWindow();
//	    });
		
	});
	
	$scope.test = function(){
		$scope.showAsideBar = !$scope.showAsideBar;
	}
	
	$scope.hideTitle = function(){
		$scope.isTitleShow = false;
	}
	
	$scope.showTitle = function(){
		$scope.isTitleShow = true;
	}
	
	$scope.isActive = function(viewLocation){
		return ($location.path().indexOf(viewLocation) > -1);
	}
	
	$scope.isGlobal = function(){
		return ($scope.session.role == "global");
	}
	
	$scope.isAdmin = function(){
		return ($scope.isGlobal() || $scope.session.role == "admin");
	}
	
	$scope.$on('changeTitle', function(event, title) {
		$scope.indexTitle = title;
		$scope.isTitleShow = true;
	});
	
	$scope.loginRoute = function(){
		return ($location.path().indexOf("login") > -1);
	}
	
	$scope.isLogged = function(){
		return $scope.session.password;
	}
	
	$scope.logout = function($event){
		$event.preventDefault();
		var confirm = $window.confirm('Deseja sair?');
		if(confirm){
			$location.path('/login');
			Session.clean();
		}
	}
	
	$scope.getYear = function() {
		var d = new Date();
		var n = d.getFullYear();
		return n;
	}
	
	$scope.zeroFill = function(number,width){
		
	    width -= number.toString().length;
	    if ( width > 0 )
	    {
	        return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
	    }
	    return number + ""; // always return a string
	}
	
	
});

