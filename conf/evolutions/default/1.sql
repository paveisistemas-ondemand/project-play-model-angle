# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table clients (
  id                        integer not null,
  client_name               varchar(255),
  constraint pk_clients primary key (id))
;

create table users (
  id                        integer not null,
  role                      varchar(255),
  client_id                 integer,
  user_name                 varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_users primary key (id))
;

create sequence clients_seq;

create sequence users_seq;

alter table users add constraint fk_users_client_1 foreign key (client_id) references clients (id);
create index ix_users_client_1 on users (client_id);



# --- !Downs

drop table if exists clients cascade;

drop table if exists users cascade;

drop sequence if exists clients_seq;

drop sequence if exists users_seq;

